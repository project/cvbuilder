CV Builder
----------

For those of you playing at home in North America, a CV is a resumé.

This module lets users create, edit, print and store them.


Requirements
------------
Drupal 4.6.x
PHP 4.3.0 or greater


Installation
------------
Please refer to the INSTALL.txt file for installation directions.


Credits
-------
 - Original author:  John Handelaar


Bugs and Suggestions
--------------------
Bug reports, support requests, feature requests, etc, should be posted to
the tracker on drupal.org.  You can reach it from this module's project
page:  http://drupal.org/node/36662

